# PHP画图

#### 介绍
php 画图工具，支持背景，多图，多文字绘制。

#### 安装教程

1.  composer require kzm/php-drawing -vvv

#### 使用说明

```php


$config = [
	'width'     => 500,
	'rate'      => 4 / 5, 								// 等比例压缩 height/width
	'bgImgPath' => 'http://iph.href.lu/500x400?bg=fff', // 背景图

	// 填充图
	'image'    => [
		[
			'path'  => 'http://iph.href.lu/100x100?bg=888',
			'width' => 100,
			'x'     => 0, 	// 位置 x
			'y'     => 0 	// 位置 y
		],
		[
			'path'   => 'http://iph.href.lu/200x300?bg=555',
			'width'  => 100,
			'rate'   => 3 / 2, 		// 等比例压缩 height/weight
			'x'      => 100, 		// 位置 x
			'y'      => 0, 			// 位置 y
			'radius' => 20 			// 圆角
		]
	],

	// 填充文字
	'string' => [
		[
			'color'      => '#999',
			'string'	 => '可爱的我(*￣︶￣)',
			'x'          => 250, 		// 位置 x
			'y'          => 30, 		// 位置 y
		],
		[
			'color'      => '#999',
			'string'     => '这是一段很长很长的话，太长了会截取的。这是一段很长很长的话，这是一段很长很长的话，这是一段很长很长的话，这是一段很长很长的话。',
			'size'       => 12, 		// 字体大小
			'x'          => 0, 			// 位置 x
			'y'          => 300, 		// 位置 y
			'width'      => 200, 		// 范围宽度
			'lineHeight' => 30, 		// 字体行高
			'lineCount'  => 3 			// 行数
		]
	]
];

$draw = new \drawing\Draw($config);
$draw->draw();


```