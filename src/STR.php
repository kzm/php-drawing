<?php

namespace drawing;

// 文字绘图对象
class STR
{
	public $string       = '';
	public $color        = '#000';
	public $size         = 20;
	public $x            = 0;
	public $y            = 0;
	public $width        = 750;
	public $lineHeight   = 40;
	public $lineCount    = 1;
	public $fontFilePath = __DIR__ . '/assets/1.ttf';

	public function __construct($params)
	{
		$obj = get_class($this);
		$keys = $this->getKeys($this);
		foreach ($params as $key => $value) {
			if (in_array($key, $keys)) {
				$this->$key = $value;
			}
		}
	}

	protected function getKeys()
	{
		$obj = get_class($this);
		$arr = get_object_vars($this);
		return array_keys($arr);
	}

	public function __set($key, $value)
	{
		$this->$key = $value;
	}

	public function __get($key)
	{
		return isset($this->$key) ? $this->$key : '';
	}
}