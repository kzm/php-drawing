<?php

namespace drawing;

class IMG
{
	public $path   = '';
	public $x      = 0;
	public $y      = 0;
	public $width  = 100;
	public $height = 0;
	public $rate   = 1;
	public $radius = 0;

	public function __construct($params)
	{
		$obj = get_class($this);
		$keys = $this->getKeys($this);
		foreach ($params as $key => $value) {
			if (in_array($key, $keys)) {
				$this->$key = $value;
			}
		}

		if (!$this->height) {
			$this->height = $this->width * ($this->rate ?: 1);
		}
	}

	protected function getKeys()
	{
		$obj = get_class($this);
		$arr = get_object_vars($this);
		return array_keys($arr);
	}

	public function __set($key, $value)
	{
		$this->$key = $value;
	}

	public function __get($key)
	{
		return isset($this->$key) ? $this->$key : '';
	}	
}